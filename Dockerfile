FROM gradle:jdk16 AS builder
COPY . /tmp/builder
WORKDIR /tmp/builder

RUN gradle build

FROM adoptopenjdk:16-jdk-hotspot
COPY --from=builder /tmp/builder/build/distributions/sbktor-0.1.1.tar /opt/sbktor.tar

RUN tar -xf /opt/sbktor.tar -C /opt &&\
mv /opt/sbktor-0.1.1 /opt/sbktor &&\
rm -f /opt/sbktor.tar

WORKDIR /opt/sbktor

EXPOSE 6543

CMD ["./bin/sbktor"]