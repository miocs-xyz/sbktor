package pw.mios.sandbox.kotlin.ktor

import com.github.mustachejava.DefaultMustacheFactory
import io.ktor.application.*
import io.ktor.http.content.*
import io.ktor.mustache.*
import io.ktor.response.*
import io.ktor.routing.*

fun Application.module() {
    install(Mustache) {
        mustacheFactory = DefaultMustacheFactory("templates")
    }
    routing {
        route("/") {
            static("assets") {
                files("assets/css")
            }
            get {
                call.respond(MustacheContent("index.hbs", mapOf("title" to "Ktor Sandbox")))
            }
        }
    }
}

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)