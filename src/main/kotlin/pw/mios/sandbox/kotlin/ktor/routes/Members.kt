package pw.mios.sandbox.kotlin.ktor.routes

import io.ktor.application.*
import io.ktor.mustache.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.membersRoting() {
    route("/members") {
        get("add") {
            call.respond(MustacheContent("add_member.hbs", null))
        }
    }
}